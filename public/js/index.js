//Assigning value to the result of a function
const balanceElement = document.getElementById("balance");
const loanElement = document.getElementById("loan");
const payElement = document.getElementById("pay");
const bankElement = document.getElementById("bank");
const workElement = document.getElementById("work");
const laptopsElement = document.getElementById("laptops");
const buyNowElement = document.getElementById("buyNow");
const priceElement = document.getElementById("price");
const featuresElement = document.getElementById("features")

//The state
let laptops = [];
let price = [];



//Fetching api from URL
fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
    .then(response => response.json())
    .then(data => laptops = data)
    .then(laptops => addLaptopsToList(laptops));


const addLaptopsToList = (laptops) => {
    laptops.forEach(x => addLaptopToList(x));
    priceElement.innerText = laptops[0].price;
    //infoboxElement.innerText = laptop[0].specs
    //const img = document.createElement("img")
    //img.src = ("https://noroff-komputer-store-api.herokuapp.com/computers") + laptops[0].image
    //imageElement.appendChild(img)
    //decriptionElement.innerText = laptops[0].description
    //nameLaptopElement.innerText = laptops[0].title
    //priceDisplayElement.innerText = '${laptops[0].price} NOK'
}

const addLaptopToList = (laptop) => {
    const laptopElement = document.createElement("option");
    laptopElement.value = laptop.id;
    laptopElement.appendChild(document.createTextNode(laptop.description));
    laptopsElement.appendChild(laptopElement);
}
//Function to handle change of laptop 
const handleLaptopChange = e => {
    const selectedLaptop = laptops[e.target.selectedIndex];
    priceElement.innerText = selectedLaptop.price;
}


laptopsElement.addEventListener("change", handleLaptopChange);

